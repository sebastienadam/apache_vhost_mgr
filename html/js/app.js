var sitesListApp = angular.module('sitesListApp', []);

sitesListApp.controller('SitesListController', function SitesListController($scope, $http) {
    $http.get("data/data.json").then(
            function (response) {
                $scope.serverName = response.data.server_name;
                $scope.availableSites = response.data.enabled_sites;
                $scope.disabledSites = response.data.disabled_sites;
            },
            function (response) {
                $scope.serverName = "Test server";
                $scope.availableSites = [];
                $scope.disabledSites = [];
            }
    );
});

