#!/usr/bin/python3
# -*- coding: utf-8 -*-

from pathlib import Path
import argparse
import json
import os
import re
import shutil
import socket
import stat
import subprocess
import sys


class ApacheVirtualHostAction():
    """ Base class for actions of the manager """
    def __init__(self, app, apache, project):
        self._cfg_apache = None
        self._path_sites_list = None
        self._cfg_project = None
        self._set_apache_config(apache)
        self._set_app_config(app)
        self._set_project_config(project)
        self._sites = None
        self._web_config = None
        self._validate_config()

    def _generate_url(self, config):
        url = '{}://{}'.format(config['protocol'],config['servername'])
        if config['port'] not in [80,443]:
            url += ':{}'.format(config['port'])
        url += '/'
        return url

    def _load_sites_list(self):
        with self._path_sites_list.open('r', encoding='utf-8') as fin:
            self._sites = json.load(fin)

    def _load_web_config(self):
        with self._cfg_apache['path_sites_list'].open('r', encoding='utf-8') as fin:
            self._web_config = json.load(fin)

    def _restart_web_server(self):
        subprocess.call(self._cfg_apache['restart'])

    def _save_sites_list(self):
        with self._path_sites_list.open('w', encoding='utf-8') as fout:
            json.dump(self._sites,fout,ensure_ascii=False,indent=2,sort_keys=True)

    def _save_web_config(self):
        if len(self._web_config['enabled_sites']) > 0:
            self._web_config['enabled_sites'].sort(key=lambda x: x['label'])
        if len(self._web_config['disabled_sites']) > 0:
            self._web_config['disabled_sites'].sort(key=lambda x: x['label'])
        with self._cfg_apache['path_sites_list'].open('w', encoding='utf-8') as fout:
            json.dump(self._web_config,fout,ensure_ascii=False,indent=2,sort_keys=True)

    def _set_apache_config(self, config):
        """ Sets the configuration for the apache server """
        self._cfg_apache = config
        self._cfg_apache['path_model'] = Path(self._cfg_apache['path_model'])
        self._cfg_apache['path_sites_available'] = Path(self._cfg_apache['path_sites_available'])
        self._cfg_apache['path_sites_enabled'] = Path(self._cfg_apache['path_sites_enabled'])
        self._cfg_apache['path_sites_list'] = Path(self._cfg_apache['path_sites_list'])

    def _set_app_config(self, config):
        """ Sets the configuration for the application """
        self._path_sites_list = Path(config['path_sites_list'])
        self._server_label = config['server_label'] if 'server_label' in config else None

    def _set_project_config(self, config):
        """ Sets the configuration for the project """
        self._cfg_project = config
        self._cfg_project['path'] = Path(self._cfg_project['path'])
        self._cfg_project['path_model'] = Path(self._cfg_project['path_model'])

    def _validate_config(self):
        """ Check if the validation is correct """
        assert sys.version_info > (3, 5), 'Minimal Python version must be 3.6'
        if os.geteuid() != 0:
            raise RuntimeError("You must be 'root' to run this code")
        if not self._cfg_apache['path_sites_available'].exists():
            raise FileNotFoundError("Can not find the Apache site configuration directory")
        if not self._cfg_apache['path_sites_enabled'].exists():
            raise FileNotFoundError("Can not find the Apache sites activation directory")
        if not self._cfg_apache['path_model'].exists():
            raise FileNotFoundError("Can not find the site configuration model")
        if not self._cfg_project['path'].exists():
            raise FileNotFoundError("Can not find the project directory")
        if not self._cfg_project['path_model'].exists():
            raise FileNotFoundError("Can not find the index page model for project")

    def Generate(self):
        """ Do the job """
        pass


class ApacheVirtualHostActionCreate(ApacheVirtualHostAction):
    """ Action class used to create a new virtual host. """
    def __init__(self, app, apache, project, args):
        self._config = None
        self._hosts = None
        self._set_args_config(args)
        ApacheVirtualHostAction.__init__(self, app, apache, project)

    def _env_has_ports(self, envs):
        return ''.join(envs).count(':') == len(envs)

    def _generate_config_page(self, config):
        """ Generates the apache config file for the new virtual host """
        config_available_path = self._cfg_apache['path_sites_available'] / config['configfile']
        config_enabled_path = self._cfg_apache['path_sites_enabled'] / config['configfile']
        with self._cfg_apache['path_model'].open('r', encoding='utf-8') as fin:
            content = fin.read()
            for key in ['documentroot', 'logdir', 'port', 'projectname', 'servername']:
                content = content.replace('%{}%'.format(key), str(config[key]))
            with config_available_path.open('w', encoding='utf-8') as fout:
                fout.write(content)
                config_enabled_path.symlink_to(config_available_path.absolute())

    def _generate_project_page(self, config):
        """ Generates the page shown by the new virtual host """
        project_page_path = Path(config['documentroot']) / 'index.html'
        with self._cfg_project['path_model'].open('r', encoding='utf-8') as fin:
            content = fin.read().replace('%projectname%',config['projectname'])
            with project_page_path.open('w', encoding='utf-8') as fout:
                fout.write(content)

    def _get_config_filename(self, config):
        """ Generates a new config filename """
        return '{:>03}-{}.conf'.format(config['id'], re.sub(r'[^\w\d]+','_',config['servername']))

    def _get_new_id(self):
        """ Finds a new ID for the config """
        if self._sites is None:
            self._load_sites_list()
        id = 1
        while True:
            site = next((x for x in self._sites if x['id'] == id), None)
            if site is None:
                break
            id += 1
        return id

    def _get_project_path(self, servername):
        """ Finds the path for the new project """
        servername = re.sub(r'[^\w\d]+','_',servername)
        path = self._cfg_project['path'] / servername
        i = 1
        while path.exists():
            path = self._cfg_project['path'] / '{}_{}'.format(servername,i)
            i += 1
        return path

    def _make_dir(self, path):
        """Create a directory with (I hope) the correct permissions"""
        try:
            path.mkdir(parents=True)
        except FileExistsError:
            pass
        shutil.chown(str(path.resolve()), user=self._cfg_apache['user'], group=self._cfg_apache['group'])
        os.chmod(str(path.resolve()), stat.S_IRWXU+stat.S_IRWXG+stat.S_IRWXO)

    def _set_args_config(self, config):
        """ Sets the configuration for cli arguments """
        self._config = config

    def _validate_config(self):
        """ Check if the validation is correct """
        ApacheVirtualHostAction._validate_config(self)
        # TODO: check for servername contain 'default'
        # no parameters given
        if self._config['env'] is None and self._config['servername'] is None and self._config['port'] is None:
            raise RuntimeError('Invalid parameters given, please refer to the manual')
        # env given, control ports
        if self._config['env'] is not None and not self._validate_env_ports(self._config['env']):
            raise RuntimeError('Invalid parameters given, please refer to the manual')
        # env given with ports and port defined
        if self._config['port'] is not None and self._config['env'] is not None and self._env_has_ports(self._config['env']):
            raise RuntimeError('Invalid parameters given, please refer to the manual')
        # prepare hosts generation
        hostname = socket.gethostname().lower()
        if self._config['servername'] is None:
            self._config['servername'] = hostname
        if self._config['projectname'] is None:
            self._config['projectname'] = self._config['servername']
        self._config['servername'] = self._config['servername'].lower()
        if self._config['port'] is None:
            self._config['port'] = 80
        else:
            self._config['projectname'] += ' :{}'.format(self._config['port'])
        if self._config['port'] == 443:
            self._config['protocol'] = 'https'
        else:
            self._config['protocol'] = 'http'
        if not re.match('^[\w_\.0-9-]+$',self._config['servername']):
            raise RuntimeError('Servername must only contain alphanumeric characters')
        if self._config['env'] is None:
            if self._config['servername'] in [hostname, 'localhost'] and self._config['port'] == 80:
                raise RuntimeError('You cannot reconfigure main server')
            self._hosts = [{
                'servername' : self._config['servername'],
                'projectname' : self._config['projectname'],
                'port' : self._config['port'],
                'protocol' : self._config['protocol'],
                'projectpath' : self._get_project_path(self._config['servername'])
            }]
        else:
            self._hosts = []
            for env in self._config['env']:
                if not re.match('^[\w_\.0-9-:]+$',env):
                    raise RuntimeError('Env must only contain alphanumeric characters')
                if ':' in env:
                    env_name, port = env.split(':')
                    servername = self._config['servername']
                else:
                    env_name = env
                    port = self._config['port']
                    servername = '{}.{}'.format(env_name,self._config['servername'])
                if servername in [hostname, 'localhost'] and port == 80:
                    raise RuntimeError('You cannot reconfigure main server')
                host = {
                    'servername' : servername,
                    'projectname' : '{} ({})'.format(self._config['projectname'],env_name),
                    'port' : port,
                    'protocol' : self._config['protocol'],
                    'env' : env_name,
                    'projectpath' : self._get_project_path(self._config['servername'])
                }
                self._hosts.append(host)

    def _validate_env_ports(self, envs):
        nb_ports = 0
        for env in envs:
            nb_col = env.count(':')
            if not nb_col in [0,1]:
                return false
            if nb_col == 1:
                (e,p) = env.split(':')
                if not p.isdigit():
                    return False
            nb_ports += nb_col
        return nb_ports in [0,len(envs)]

    def Generate(self):
        """ Do the job """
        self._load_sites_list()
        self._load_web_config()
        for host in self._hosts:
            host['status'] = 'enabled'
            host['id'] = self._get_new_id()
            host['configfile'] = self._get_config_filename(host)
            self._make_dir(host['projectpath'])
            if 'env' in host:
                documentroot = host['projectpath'] / 'www_{}'.format(host['env'])
                log_dir = host['projectpath'] / 'log_{}'.format(host['env'])
                del host['env']
            else:
                documentroot = host['projectpath'] / 'www'
                log_dir = host['projectpath'] / 'log'
            del host['projectpath']
            self._make_dir(documentroot)
            self._make_dir(log_dir)
            host['documentroot'] = str(documentroot.resolve())
            host['logdir'] = str(log_dir.resolve())
            self._generate_config_page(host)
            self._generate_project_page(host)
            self._sites.append(host)
            site_config = {
                'id' : host['id'],
                'label' : host['projectname'],
                'url' : self._generate_url(host)
            }
            self._web_config['enabled_sites'].append(site_config)
        self._save_sites_list()
        self._save_web_config()
        self._restart_web_server()


class ApacheVirtualHostActionDelete(ApacheVirtualHostAction):
    """ Action class used to delete a virtual host and all its files. """
    def __init__(self, app, apache, project, args):
        self._config_id = None
        self._selected_site = None
        self._set_args_config(args)
        ApacheVirtualHostAction.__init__(self, app, apache, project)

    def _set_args_config(self, config):
        """ Sets the configuration for cli arguments """
        self._config_id = config['delete']

    def _validate_config(self):
        """ Check if the validation is correct """
        ApacheVirtualHostAction._validate_config(self)
        self._load_sites_list()
        self._selected_site = next((x for x in self._sites if x['id'] == self._config_id), None)
        if self._selected_site is None:
            raise RuntimeError('Invalid virtual host ID ({})'.format(self._config_id))

    def Generate(self):
        """ Do the job """
        a = None
        print('This action will delete the selected virtual host and all its files irreversibly.')
        while True:
            a = input('Are you sure you want to continue? [y/N] ').lower()
            if a == '':
                a = 'n'
            if a in ['y', 'n']:
                break
            print("Pleas answere by 'y' or 'n'.")
        if a == 'n':
            print('Canceled')
        else:
            available_conf_path = self._cfg_apache['path_sites_available'] / self._selected_site['configfile']
            enable_conf_path = self._cfg_apache['path_sites_enabled'] / self._selected_site['configfile']
            log_dir = Path(self._selected_site['logdir'])
            web_dir = Path(self._selected_site['documentroot'])
            self._load_web_config()
            key = '{}_sites'.format(self._selected_site['status'])
            current_web = next((x for x in self._web_config[key] if x['id'] == self._config_id), None)
            if current_web is not None:
                self._web_config[key].remove(current_web)
            self._save_web_config()
            self._sites.remove(self._selected_site)
            self._save_sites_list()
            if enable_conf_path.exists():
                enable_conf_path.unlink()
            if available_conf_path.exists():
                available_conf_path.unlink()
            if log_dir.exists():
                shutil.rmtree(str(log_dir.resolve()), ignore_errors=True)
            #if web_dir.exists():
            #    shutil.rmtree(str(web_dir.resolve()), ignore_errors=True)
            self._restart_web_server()


class ApacheVirtualHostActionDisable(ApacheVirtualHostAction):
    """ Action class used to disable a virtual host """
    def __init__(self, app, apache, project, args):
        self._config_id = None
        self._selected_site = None
        self._set_args_config(args)
        ApacheVirtualHostAction.__init__(self, app, apache, project)

    def _set_args_config(self, config):
        """ Sets the configuration for cli arguments """
        self._config_id = config['disable']

    def _validate_config(self):
        """ Check if the validation is correct """
        ApacheVirtualHostAction._validate_config(self)
        self._load_sites_list()
        self._selected_site = next((x for x in self._sites if x['id'] == self._config_id), None)
        if self._selected_site is None:
            raise RuntimeError('Invalid virtual host ID ({})'.format(self._config_id))

    def Generate(self):
        """ Do the job """
        config_file = self._cfg_apache['path_sites_enabled'] / self._selected_site['configfile']
        if config_file.exists():
            config_file.unlink()
        self._selected_site['status'] = 'disabled'
        self._save_sites_list()
        self._load_web_config()
        current_web = next((x for x in self._web_config['enabled_sites'] if x['id'] == self._config_id), None)
        if current_web is not None:
            self._web_config['enabled_sites'].remove(current_web)
            del current_web['url']
            self._web_config['disabled_sites'].append(current_web)
        self._save_web_config()
        self._restart_web_server()


class ApacheVirtualHostActionList(ApacheVirtualHostAction):
    """ Action class used to list the available sites """
    def Generate(self):
        """ Do the job """
        if self._cfg_apache['path_sites_list'].exists():
            sites = {}
            labels = {
                'enabled_sites'  : 'Enabled sites',
                'disabled_sites' : 'Disabled sites'
            }
            with self._cfg_apache['path_sites_list'].open('r', encoding='utf-8') as fin:
                sites = json.load(fin)
            for key in labels.keys():
                if key in sites:
                    print('{}:'.format(labels[key]))
                    if len(sites[key]) == 0:
                        print('*** No virtual host ***')
                    else:
                        for site in sites[key]:
                            print('({:>3}) {}'.format(site['id'],site['label']))
                            if 'url' in site:
                                print('\t=> {}'.format(site['url']))
                print('')
        else:
            print('No site configurated!')


class ApacheVirtualHostActionReactivate(ApacheVirtualHostAction):
    """ Action class used to reactivate a virtual host """
    def __init__(self, app, apache, project, args):
        self._config_id = None
        self._selected_site = None
        self._set_args_config(args)
        ApacheVirtualHostAction.__init__(self, app, apache, project)

    def _set_args_config(self, config):
        """ Sets the configuration for cli arguments """
        self._config_id = config['activate']

    def _validate_config(self):
        """ Check if the validation is correct """
        ApacheVirtualHostAction._validate_config(self)
        self._load_sites_list()
        self._selected_site = next((x for x in self._sites if x['id'] == self._config_id), None)
        if self._selected_site is None:
            raise RuntimeError('Invalid virtual host ID ({})'.format(self._config_id))

    def Generate(self):
        """ Do the job """
        available_conf_path = self._cfg_apache['path_sites_available'] / self._selected_site['configfile']
        enable_conf_path = self._cfg_apache['path_sites_enabled'] / self._selected_site['configfile']
        if not enable_conf_path.exists():
            enable_conf_path.symlink_to(available_conf_path.absolute())
        self._selected_site['status'] = 'enabled'
        self._save_sites_list()
        self._load_web_config()
        current_web = next((x for x in self._web_config['disabled_sites'] if x['id'] == self._config_id), None)
        if current_web is not None:
            self._web_config['disabled_sites'].remove(current_web)
            current_web['url'] = self._generate_url(self._selected_site)
            self._web_config['enabled_sites'].append(current_web)
        self._save_web_config()
        self._restart_web_server()


class ApacheVirtualHostActionRebuild(ApacheVirtualHostAction):
    """ Action class used to rebuild the configuration files """
    def Generate(self):
        """ Do the job """
        self._sites = []
        # rebuild the config of the application
        for file in sorted(self._cfg_apache['path_sites_available'].glob('*.conf')):
            if 'default' in file.name or re.match('^\d+-[\w_-]+\.conf$',file.name) is None:
                print('passing {}'.format(file.name))
                continue
            with file.open('r', encoding='utf-8') as fin:
                config = {}
                config['configfile'] = file.name
                config['id'] = int(file.name[:3])
                config['projectname'] = file.name[4:-5]
                for line in fin:
                    projectname_search = re.search('^# project name: (.+?)$',line,re.I)
                    if projectname_search is not None:
                        config['projectname'] = projectname_search.group(1)
                        continue
                    port_search = re.search("<virtualhost.+?:(\d+).*?>",line,re.I)
                    if port_search is not None:
                        config['port'] = int(port_search.group(1))
                        if config['port'] == 443:
                            config['protocol'] = 'https'
                        else:
                            config['protocol'] = 'http'
                        continue
                    servername_search = re.search("servername.+?([\w\.-]+)",line,re.I)
                    if servername_search is not None:
                        config['servername'] = servername_search.group(1)
                        continue
                    documentroot_search = re.search("documentroot +(/[\w\./-]+)( ?#.+?)?$",line,re.I)
                    if documentroot_search is not None:
                        config['documentroot'] = documentroot_search.group(1)
                        continue
                    log_search = re.search("errorlog.+?([\w\.\/-]+)( ?#.+?)?$",line,re.I)
                    if log_search is not None:
                        config['logdir'] = str(Path(log_search.group(1)).parent)
                        continue
                if (self._cfg_apache['path_sites_enabled']/file.name).exists():
                    config['status'] = 'enabled'
                else:
                    config['status'] = 'disabled'
                self._sites.append(config)
        self._save_sites_list()
        # rebuild the config of the main server page
        self._web_config = {"enabled_sites" : [], "disabled_sites" : []}
        if self._server_label is not None:
            self._web_config['server_name'] = self._server_label
        for config in self._sites:
            site_config = {
                'id' : config['id'],
                'label' : config['projectname']
            }
            if config['status'] == 'enabled':
                site_config['url'] = self._generate_url(config)
            key = '{}_sites'.format(config['status'])
            self._web_config[key].append(site_config)
        self._save_web_config()
        print("Configuration rebuilded")
        print("Config in '{}'".format(self._path_sites_list.resolve()))
        print("Site config in '{}'".format(self._cfg_apache['path_sites_list'].resolve()))


class ApacheVirtualHostManager():
    """ Front end class for the manager """
    def __init__(self, args, app, apache, project):
        if args['activate'] is not None:
            self._action = ApacheVirtualHostActionReactivate(app, apache, project, args)
        elif args['delete'] is not None:
            self._action = ApacheVirtualHostActionDelete(app, apache, project, args)
        elif args['disable'] is not None:
            self._action = ApacheVirtualHostActionDisable(app, apache, project, args)
        elif args['list']:
            self._action = ApacheVirtualHostActionList(app, apache, project)
        elif args['rebuild']:
            self._action = ApacheVirtualHostActionRebuild(app, apache, project)
        else:
            self._action = ApacheVirtualHostActionCreate(app, apache, project, args)

    def Generate(self):
        """ Do the job """
        self._action.Generate()


def GetArgs():
    """ Retrieve command line parametrers """
    parser = argparse.ArgumentParser(description='Apache virtual host adder')
    parser.add_argument('-a', '--activate', help='Configuration ID that must be reactivated', type=int, required=False)
    parser.add_argument('-d', '--disable', help='Configuration ID that must be disabled', type=int, required=False)
    parser.add_argument('-D', '--delete', help='Configuration ID that must be deleted', type=int, required=False)
    parser.add_argument('-e', '--env', help='Environments to create in the project', nargs='+', required=False)
    parser.add_argument('-l', '--list', help='List the virtual hosts', action='store_true', required=False)
    parser.add_argument('-n', '--projectname', help='Project name to create', required=False)
    parser.add_argument('-p', '--port', help='Server port to create', type=int, required=False)
    parser.add_argument('-r', '--rebuild', help='Rebuild the list of virtual hosts', action='store_true', required=False)
    parser.add_argument('-s', '--servername', help='Server name to create', required=False)
    return vars(parser.parse_args())


def LoadConfig():
    """ Load the script config file """
    config = None
    path = Path('vhostmgr_include/config.json')
    config = json.loads(path.read_text())
    return config


def Main():
    """ Entry script """
    config = LoadConfig()
    config.update({'args': GetArgs()})
    avhm = ApacheVirtualHostManager(**config)
    avhm.Generate()


if __name__ == '__main__':
    Main()
